resource "aws_api_gateway_rest_api" "rest_api" {
  name = "tf-rest-api"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
  disable_execute_api_endpoint = true
}

# aws_api_gateway_resource is path
# aws_api_gateway_method is HTTP method
resource "aws_api_gateway_resource" "time_resource" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  parent_id   = aws_api_gateway_rest_api.rest_api.root_resource_id # path = /

  path_part = "time" # path = ${stage_name} + / + time = /prod/time
}

resource "aws_api_gateway_method" "time_method" {
  rest_api_id   = aws_api_gateway_rest_api.rest_api.id
  resource_id   = aws_api_gateway_resource.time_resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "time_integration" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.time_resource.id
  http_method = aws_api_gateway_method.time_method.http_method

  integration_http_method = "POST"
  type                    = "AWS" # if you use event argument within lambda use AWS_PROXY
  uri                     = aws_lambda_function.test_lambda.invoke_arn
  content_handling        = "CONVERT_TO_TEXT"
}

# not sure
resource "aws_api_gateway_method_response" "time" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.time_resource.id
  http_method = aws_api_gateway_method.time_method.http_method

  status_code = "200"

  response_models = {
    "application/json" = "Empty"
  }
}

# not sure
resource "aws_api_gateway_integration_response" "time" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.time_resource.id
  http_method = aws_api_gateway_method.time_method.http_method

  status_code = aws_api_gateway_method_response.time.status_code
  # since there is no direct link
  depends_on = [aws_api_gateway_integration.time_integration]
}

# allow api gateway to call lambda
resource "aws_lambda_permission" "time" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.rest_api.execution_arn}/*"
}

# deploy whenever triggers change
resource "aws_api_gateway_deployment" "time" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id

  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_method.time_method.id,
      aws_api_gateway_integration.time_integration.id,
      aws_api_gateway_method_response.time,
      aws_api_gateway_integration_response.time,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "lambda" {
  deployment_id = aws_api_gateway_deployment.time.id
  rest_api_id   = aws_api_gateway_rest_api.rest_api.id
  stage_name    = "prod"
}
