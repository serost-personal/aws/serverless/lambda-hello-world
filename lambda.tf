data "aws_iam_policy_document" "allow_s3_readonly" {
  statement {
    effect    = "Allow"
    actions   = ["s3:ListAllMyBuckets"]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
  inline_policy {
    name   = "allow_s3_readonly"
    policy = data.aws_iam_policy_document.allow_s3_readonly.json
  }
}

data "archive_file" "lambda" {
  type        = "zip"
  source_dir  = "./code"
  excludes    = [".venv", ".gitignore"]
  output_path = "hello-world.zip"
}

resource "aws_lambda_function" "test_lambda" {
  filename      = "./hello-world.zip"
  function_name = "hello-world"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "main.lambda_main"

  source_code_hash = data.archive_file.lambda.output_base64sha256

  runtime = "python3.9"

  environment {
    variables = {
      foo = "bar"
    }
  }
}
