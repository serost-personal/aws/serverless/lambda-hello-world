terraform {
  required_providers {
    aws = {}
  }
}

provider "aws" {
  region = "eu-north-1"
}
