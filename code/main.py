import os
import json
from boto3 import client

def lambda_main(event, context):
    s3 = client('s3') # will read from env
    buckets = s3.list_buckets()
    return json.dumps(buckets['Buckets'], default=str)