
variable "api_gateway_domain" {}
variable "api_gateway_domain_certificate_arn" {}

resource "aws_api_gateway_domain_name" "this" {
  domain_name              = "api.${var.api_gateway_domain}"
  regional_certificate_arn = var.api_gateway_domain_certificate_arn
  security_policy          = "TLS_1_2"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

data "aws_route53_zone" "domain" {
  name         = var.api_gateway_domain
  private_zone = false
}

resource "aws_route53_record" "apigateway" {
  name    = aws_api_gateway_domain_name.this.domain_name
  type    = "A"
  zone_id = data.aws_route53_zone.domain.id

  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.this.regional_domain_name
    zone_id                = aws_api_gateway_domain_name.this.regional_zone_id
  }
}

resource "aws_api_gateway_base_path_mapping" "lambda" {
  domain_name = aws_api_gateway_domain_name.this.domain_name
  api_id      = aws_api_gateway_rest_api.rest_api.id
  stage_name  = aws_api_gateway_stage.lambda.stage_name
  base_path   = null # null means root (api.domain.com/)
}

output "deployed_onto" {
  value = "https://api.${var.api_gateway_domain}/time"
}
